package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("AB Testing")
public class ABFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Validar a AB Testing")
	public void validar_ab() throws ExecutionException {
		
		given_("Dado que eu esteja na home da TheInternet").
		when_("Quando eu clicar em AB Testing").
		then_("Então a página será exibida").
		execute_();
	}
}