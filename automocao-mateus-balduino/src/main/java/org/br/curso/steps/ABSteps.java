package org.br.curso.steps;

import org.br.curso.pages.ABPage;
import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

@DesignSteps
public class ABSteps {
	
	HomePage homePage = getPage_(HomePage.class);
	ABPage abPage = getPage_(ABPage.class);
	
	@Step("Dado que eu esteja na home da TheInternet")
	public void estar_na_home() {
		homePage.validar_home();
	}

	@Step("Quando eu clicar em AB Testing")
	public void clicar_abtesting() throws ElementFindException{
		homePage.acessar_ab();
	}
	
	@Step("Então a página será exibida")
	public void validar_ab() {
		abPage.validar_pageAB();
	}
}
