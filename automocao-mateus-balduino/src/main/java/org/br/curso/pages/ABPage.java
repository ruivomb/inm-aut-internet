package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class ABPage extends PageBase {
	
	VirtualElement txtAB = getElementByXPath("//h3");
	
	LoggerHelper logger = new LoggerHelper(ABPage.class);
	
	public void validar_pageAB() {
		Assert.assertTrue(elementExists(txtAB));
		logger.info("Página AB Testing validada.", true);
		}
}