package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class HomePage extends PageBase {
	
	VirtualElement txtTitulo = getElementByXPath("//h1"),
				   lnkAB = getElementByXPath("//a[@href=\"/abtest\"]");
	
	LoggerHelper logger = new LoggerHelper(HomePage.class);
	
	public void validar_home() {
		Assert.assertTrue(elementExists(txtTitulo));
		logger.info("Pagina validada.", true);	
	}
	
	public void acessar_ab() throws ElementFindException {
		waitUntilElementExists(lnkAB, 10);
		lnkAB.click();
	}
}
